#/bin/bash
ssh_clone_url=$1
latest=`git tag --sort=-creatordate`

if [ -n "$latest" ]; then
  finalver=`echo $latest | grep -o "[^v].*"`
else
  finalver="1.0.0" #base number to start bumping
fi

bumpversion=v`python3 -c 'import sys; import semver; print (semver.bump_patch(sys.argv[1]))' $finalver`

git config user.name automation@gitlab
git config user.email automation@gitlab.com
git tag -a $bumpversion -m"version $bumpversion"
git remote set-url origin $ssh_clone_url
git push origin $bumpversion
echo $bumpversion
