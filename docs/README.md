[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
# DevOps Assignment (Versioning)

This is a check-in for DevOps engineers related to the assignment/tasks.


- - -
 

## Pre-requisites

You will need the following installed:

- `docker` for image building/publishing (check with `docker version`)
- `git` for source control (check with `git -v`)

You will also need to clone the below Gitlab repository
- URL - https://gitlab.com/mayday77/devops-check-in-003
- SSH - git@gitlab.com:mayday77/devops-check-in-003.git
- https - https://gitlab.com/mayday77/devops-check-in-003.git


## Get Started

1. Clone this repository
2. Create your own repository on GitLab
3. Set your local repository's remote to point to your GitLab repository
4. Make your changes locally according to the tasks below if applicable
5. Push to your GitLab repository



- - -

# Versioning


## Task

When referring to problems, we often use a version number. The (arguably) leading way to do this is via semver (eg. 1.15.2).
In the existing pipeline, add additional scripting to bump the version of this repository using Git tags, and add this version to the produced Docker image.
> Note: The pipeline illustrated here is taken from another project: https://gitlab.com/mayday77/devops-check-in-002.

The illustration given is just a simple version bump all the time.

## Steps
1. In the pipeline code, the first stage is to get the current git tag version and bump it if applicable. The remaining stages will just build the binary and tag the docker image with the latest version.

2. Setup details as follow:
   - A script in ./scripts folder named genver.sh will get the git tag and bump the version accordingly.
   - The script make use of a python module named semver which will do the version bump. (https://github.com/python-semver/python-semver)
   - In order to create a new git tag and the ability for the pipeline to git push back to the repo, additional setup was done.
     * First generate a set of private/public keys using ssh-keygen.
     * Add the public key into as a new Deploy Key with write permissions in Settings -> Repository
       ![](.README_images/deploy_key.png)  
     * Add the private key as a variable in Settings > CI/CD
       ![](.README_images/private_key.png)
     * Set the current ssh git repo URL as a variable as well
       ![](.README_images/git_clone_url.png)
       
3. We will start with running the pipeline for the first time or do a commit to the master branch.
   The pipeline will automatically create the first tag if none exists and push it back to the remote repository.
   Issue the following command and there will be no tags defined locally.
   ```
   git tag
   ```
   
   From the Gitlab GUI repository page, no tags are defined as well.
   
   ![](.README_images/gui_tag.png)
   
4. Run the pipeline from the Gitlab GUI.

   ![](.README_images/run_pipeline.png)

## Result
1. Once the pipeline is finished, check the git tags from the Gitlab GUI repository page.

   ![](.README_images/git_tag_1.png)
   
2. On the local working machine, issue a git pull. And a new tag has been pulled.
   ```
   > git pull
   remote: Enumerating objects: 1, done.
   remote: Counting objects: 100% (1/1), done.
   remote: Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
   Unpacking objects: 100% (1/1), done.
   From gitlab.com:mayday77/devops-check-in-003
    * [new tag]         v1.0.1     -> v1.0.1
   ```
3. The pipeline build docker image stage produces the job artifact, download the artifact and check the docker image tags.
   The build-docker-image stage console log will show the docker tag command as well.
   
   ![](.README_images/docker_tag.png)
   
   Once the artifact is downloaded, use the docker command to load and view the image.
   ```
   docker load -i pinger.tar
   docker images
   ```
   ![](.README_images/docker_images.png)
   
   
## Test
1. Run the pipeline again from the Gitlab GUI using master branch.

   ![](.README_images/pipeline_run2.png)

2. Once the pipeline is finished, check the git tags from the Gitlab GUI repository page.

   ![](.README_images/repo_tag2.png)

3. On the local working machine, issue a git pull. And another new tag has been pulled.
   ```
   > git pull
   remote: Enumerating objects: 1, done.
   remote: Counting objects: 100% (1/1), done.
   remote: Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
   Unpacking objects: 100% (1/1), done.
   From gitlab.com:mayday77/devops-check-in-003
    * [new tag]         v1.0.2     -> v1.0.2
   ```

4. Download the docker image job artifact and check the tags again.
   ```
   docker load -i pinger.tar
   docker images
   ```
   ![](.README_images/docker_images2.png)

5. You can repeat the same tests to verify further the next version bump.
- - -

# License

Code is licensed under the [MIT license](./LICENSE).

Content is licensed under the [Creative Commons 4.0 (Attribution) license](https://creativecommons.org/licenses/by-nc-sa/4.0/).



- - -

